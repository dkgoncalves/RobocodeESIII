package team_3.robot2_Deadpool;

import java.awt.Color;
import robocode.AdvancedRobot;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.RobotDeathEvent;
import robocode.ScannedRobotEvent;

public class Deadpool extends AdvancedRobot {

    private final String[] dontAttack = {"BorderGuard", "Storm", "Punisher"};

    private String tracked = null;
    double trackedDistance = 10000;

    // variavel usada pra verificar quando robo esta se movendo pra frente ou para trás
    boolean runningAhead = true;

    // variavel usada na verificacao se o walls foi encontrado vivo na arena
    boolean wallsDead = false;

    // vezes que o radar encontrou um inimigo quando estava procurando pelo walls
    int wallsSearchCount = 0;

    @Override
    public void run() {
	double maxArea = getBattleFieldHeight() - 150; //

	setMaxVelocity(8);
	setAdjustGunForRobotTurn(true);
	setAdjustRadarForRobotTurn(true);
	setAdjustRadarForGunTurn(true);

	setBodyColor(Color.black);
	setGunColor(Color.black);
	setRadarColor(Color.green);
	setBulletColor(Color.green);

	turnRight(-getHeading() + 90);

	double distance = maxArea - getX();

	setTurnRadarRight(360);

	while (true) {

	    // se nao esta mirando nenhum inimigo
	    if (tracked == null) {
		setTurnRight(80);
		setAhead(distance);
	    }

	    setTurnRadarRight(360);

	    execute();
	}
    }

    @Override
    public void onHitRobot(HitRobotEvent e) {
	if (e.getBearing() > -90 && e.getBearing() < 90) {
	    back(100);
	} else {
	    ahead(100);
	}
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent event) {

	// Retorna não fazendo nada caso o robo scaneado seja um aliado
	if (isAllied(event.getName())) {
	    return;
	}

	// se o walls ainda nao foi encontrado mas aparentemente nao esta dado como morto
	if (wallsDead == false) {
	    if (!event.getName().toLowerCase().contains("walls")) {
		return;
	    }

	    wallsSearchCount++;
	    // se encontrar 10 inimigos e nenhum for o walls, desistir de buscar por ele dizendo que ele esta morto
	    if (wallsSearchCount >= 10) {
		wallsDead = true;
	    }
	}

	// Se ainda nao tiver nenhum inimigo na mira ou 
	// Se e a distancia do inimigo scaneado for menor que o ultimo robo focado
	if (tracked == null || event.getDistance() < trackedDistance || event.getName().equals(tracked)) {
	    // Seta o nome do robo scaneado para ser focado
	    tracked = event.getName();
	    // guarda a distancia do robo scaneado
	    trackedDistance = event.getDistance();
	}

	// verifica se o robo scaneado é o robo que deve ser focado
	if (event.getName().equals(tracked)) {
	    // angulo em que o canhao deve ficar para mirar o inimigo
	    double enemyAngle = getHeading() + event.getBearing();

	    if (enemyAngle >= 360) {
		enemyAngle -= 360;
	    }

	    // normaliza o angulo para dar a menor volta possivel
	    double turnGunAngle = normalizeBearing(enemyAngle - getGunHeading());
	    setTurnRight(normalizeBearing(enemyAngle - getHeading()));
	    if (getDistanceRemaining() <= 15) {
		setAhead(event.getDistance() - 50);
		runningAhead = true;
	    }
	    double turnRadarAngle = enemyAngle - getRadarHeading();

	    if (turnGunAngle >= 360) {
		turnGunAngle -= 360;
	    }

	    if (turnRadarAngle >= 360) {
		turnRadarAngle -= 360;
	    }
	    turnRadarRight(normalizeBearing(turnRadarAngle));

	    if (getGunHeat() >= 0 && Math.abs(getGunTurnRemaining()) < 2) {
		if (event.getDistance() <= 150) {
		    setFire(3);
		}
		if (event.getDistance() <= 350) {
		    setFire(2);
		}
		if (event.getDistance() > 350 && event.getDistance() <= 1000) {
		    setFire(1);
		}
	    }
	    
	    if (getGunTurnRemaining() <= 0) {
		setTurnGunRight(turnGunAngle);
	    }
	    execute();

	}
    }

    // normaliza o  bearing entre +180 e -180 graus
    double normalizeBearing(double angle) {
	while (angle > 180) {
	    angle -= 360;
	}
	while (angle < -180) {
	    angle += 360;
	}
	return angle;
    }

// Metodo utilizado no evento onScannedRobot: verifica se o robo scaneado é aliado ou nao
    private boolean isAllied(String scannedRobotName) {
	for (String robot : dontAttack) {
	    if (scannedRobotName.toLowerCase().contains(robot.toLowerCase())) {
		return true;
	    }
	}
	return false;
    }

    // quando um robo morre na arena
    @Override
    public void onRobotDeath(RobotDeathEvent event) {
	// se o robo que morreu for o robo que estamos mirando,
	// remove o robo do foco
	if (event.getName().equals(tracked)) {
	    tracked = null;
	    trackedDistance = 1000;
	    setTurnRadarRight(360);
	}
	if (event.getName().toLowerCase().contains("walls")) {
	    wallsDead = true;
	}
    }

    @Override
    public void onHitWall(HitWallEvent event) {
	if (runningAhead) {
	    setBack(200);
	    runningAhead = false;
	} else {
	    setAhead(200);
	    runningAhead = true;
	}
    }

}
