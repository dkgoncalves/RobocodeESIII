package team_3.robot3_Punisher;

import java.awt.Color;
import robocode.*;
import java.util.concurrent.ThreadLocalRandom;
//import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html
/**
 * Robot3 - a robot by Arthur
 */
public class Punisher extends AdvancedRobot {

    private final String[] dontAttack = {"BorderGuard", "Storm", "Deadpool"};

    //verifica o tamanho da arena
    int borda;
    double maxHeight;
    double maxWidth;

    ScannedRobotEvent enemy;                                                    //salva os dados do robo detectado
    long scanTempo;                                                             //impede tiros até um inimigo ser detectado

    int hitCounter = 0;                                                         //conta os tiros recebidos até mudar o tipo de movimento
    int movimentacaoTipo = 0;                                                   //controla se a movimentação A ou B esta sendo usada no momento

    /**
     * run: ARobo2's default behavior
     */
    public void run() {
	setBodyColor(Color.black);
	setGunColor(Color.black);
	setRadarColor(Color.green);
	setBulletColor(Color.green);
	
	// Initialization of the robot should be put here

	// After trying out your robot, try uncommenting the import at the top,
	// and the next line:
	// setColors(Color.red,Color.blue,Color.green); // body,gun,radar
	// Robot main loop
	scanTempo = 0;                                                          //impede tiros até um inimigo ser detectado

	//verifica o tamanho da arena
	borda = getSentryBorderSize();
	maxHeight = getBattleFieldHeight() - borda;
	maxWidth = getBattleFieldWidth() - borda;

	//gera os pontos do movimento tipo A
	double robotX = getX();
	double robotY = getY();
	//double[] movimentoA = setupPoints(robotX, robotY);
        double[] movimentoA = {300,300,500,200,300,400};

	//gera os pontos do movimento tipo B
	/*double[] movBCentro = {ThreadLocalRandom.current().nextDouble(robotX - 500, robotX + 500), ThreadLocalRandom.current().nextDouble(robotY - 500, robotY + 500)};
	double[] movimentoB = setupPoints(movBCentro[0], movBCentro[1]);*/
        double[] movimentoB = {800,800,400,800,800,400};

	int pontoAtivo = 0;                                                     //determina o proximo ponto alvo

	/*setAdjustRadarForRobotTurn(true);
	setAdjustRadarForGunTurn(true);
	setAdjustGunForRobotTurn(true);*/

	while (true) {
	    //int pontoAntigo = pontoAtivo;
	    //troca entre os dois tipos de movimentação e seus varios pontos
	    if (movimentacaoTipo == 0) {
		switch (pontoAtivo) {
		    case 0:
			pontoAtivo = moveAndCheck(movimentoA[0], movimentoA[1], pontoAtivo);
			break;
		    case 1:
			pontoAtivo = moveAndCheck(movimentoA[2], movimentoA[3], pontoAtivo);
			break;
		    default:
			pontoAtivo = moveAndCheck(movimentoA[4], movimentoA[5], pontoAtivo);
			break;
		}
	    } else {
		switch (pontoAtivo) {
		    case 0:
			pontoAtivo = moveAndCheck(movimentoB[0], movimentoB[1], pontoAtivo);
			break;
		    case 1:
			pontoAtivo = moveAndCheck(movimentoB[2], movimentoB[3], pontoAtivo);
			break;
		    default:
			pontoAtivo = moveAndCheck(movimentoB[4], movimentoB[5], pontoAtivo);
			break;
		}
	    }
	    // Retorna não fazendo nada caso o robo scaneado seja um aliado
	    
	    setTurnRadarRight(360);                                             //gira o radar procurando por robos

	    execute();

	}
    }

    /**
     * onScannedRobot: What to do when you see another robot
     *
     * @param e
     */
    @Override
    public void onScannedRobot(ScannedRobotEvent e) {
	enemy = e;                                                              //salva os dados do robo detectado
	scanTempo = e.getTime();                                                //salva o turno no qual o robo foi detectado
        if (enemy != null) {
            if (isAllied(enemy.getName()) == false) {          //escolhe o tipo de tiro baseado na distancia do robo detectado
                setAdjustGunForRobotTurn(true);
                setTurnGunRight(getHeading() - getGunHeading() + e.getBearing());       //mira no robo detectado
                if (enemy.getDistance() > 300) {
                    setFire(1);
                    setFire(1);
                } else {
                    setFire(3);
                    setFire(3);
                }
            }
	}
    }

    /**
     * onHitByBullet: What to do when you're hit by a bullet
     *
     * @param e
     */
    @Override
    public void onHitByBullet(HitByBulletEvent e) {                             //conta os tiros recebidos de depois de 2 tiros troca o tipo de movimentação
	// Replace the next line with any behavior you would like
	hitCounter++;
	if (hitCounter >= 2) {
	    if (movimentacaoTipo == 0) {
		movimentacaoTipo = 1;
	    } else {
		movimentacaoTipo = 0;
	    }
	    hitCounter = 0;
	}
    }
	
	public void onHitRobot(HitRobotEvent e) {
		// If he's in front of us, set back up a bit.
		if (e.getBearing() > -90 && e.getBearing() < 90) {
			back(100);
		} // else he's in back of us, so set ahead a bit.
		else {
			ahead(100);
		}
	}

    /**
     * onHitWall: What to do when you hit a wall
     *
     * @param e
     */
    @Override
    public void onHitWall(HitWallEvent e) {                                     //impossivel de acontecer
	// Replace the next line with any behavior you would like
		back(100);
		turnRight(180);
		ahead(100);

    }

    private int moveAndCheck(double alvoX, double alvoY, int pontoAtivo) {         //verifica quando um ponto foi alcançado e começa o movimento para o proximo ponto
	int margemDeErro = 40;
	goTo((int) alvoX, (int) alvoY);
	if (alvoX < getX() + margemDeErro && alvoX > getX() - margemDeErro && alvoY < getY() + margemDeErro && alvoY > getY() - margemDeErro) {
	    pontoAtivo++;
	}
	if (pontoAtivo >= 3) {
	    pontoAtivo = 0;
	}
	return pontoAtivo;
    }

    private void goTo(int x, int y) {                                           //imported (movimenta o robo para uma coordenada)
	double a;
	setTurnRightRadians(Math.tan(
		a = Math.atan2(x -= (int) getX(), y -= (int) getY())
		- getHeadingRadians()));
	setAhead(Math.hypot(x, y) * Math.cos(a));
    }

    private double[] setupPoints(double centerX, double centerY) {                //verifica se o centro esta dentro da arena
	//Esses ifs garantem que o ponto central usado para gerar o caminho não é dentro da borda
	if (centerX < borda) {
	    centerX = borda + 50;
	}
	if (centerX > maxWidth) {
	    centerX = maxWidth - 50;
	}

	if (centerY < borda) {
	    centerY = borda + 50;
	}
	if (centerY > maxHeight) {
	    centerY = maxHeight - 50;
	}
	//envia o ponto central para gerar os 3 pontos finais da movimentação e os envia para serem salvos
	return setupPointsPart2(centerX - 200, centerX + 200, centerY - 200, centerY + 200);
    }

    private double[] setupPointsPart2(double minX, double maxX, double minY, double maxY) {       //prepara os pontos usados no movimento do robo
	//Esses ifs garantem que o robo não vai entrar na borda
	if (minX < borda) {
	    minX = borda + 50;
	}
	if (maxX > maxWidth) {
	    maxX = maxWidth - 50;
	}

	if (minY < borda) {
	    minY = borda + 50;
	}
	if (maxY > maxHeight) {
	    maxY = maxHeight - 50;
	}
	//gera aleatoriamente 3 pontos
	double movPonto[] = {ThreadLocalRandom.current().nextDouble(minX, maxX), ThreadLocalRandom.current().nextDouble(minY, maxY),
	    ThreadLocalRandom.current().nextDouble(minX, maxX), ThreadLocalRandom.current().nextDouble(minY, maxY),
	    ThreadLocalRandom.current().nextDouble(minX, maxX), ThreadLocalRandom.current().nextDouble(minY, maxY)};

	return movPonto;
    }

    // Metodo utilizado no evento onScannedRobot: verifica se o robo scaneado é aliado ou nao
    private boolean isAllied(String scannedRobotName) {
	for (String robot : dontAttack) {
	    if (scannedRobotName.toLowerCase().contains(robot.toLowerCase())) {
		return true;
	    }
	}
	return false;
    }

}