package team_3.robot1_Storm;

import robocode.*;
import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html
/**
 * Storm - a robot by (your name here)
 */
public class Storm extends AdvancedRobot {

    /**
     * run: Storm's default behavior
     */
    private final String[] dontAttack = {"BorderGuard", "Deadpool", "Punisher"};
	boolean peek;
	ScannedRobotEvent enemy; 
	long scanTempo;
    @Override
    public void run() {

	setBodyColor(Color.black);
	setGunColor(Color.black);
	setRadarColor(Color.green);
	
	setBulletColor(Color.green);

	double wallHeightLimit = getBattleFieldHeight() -180;
	double wallWidthLimit = getBattleFieldWidth() -180;

	
	while (true) {
	    	ahead(400);
			turnRight(120);
			setTurnGunRightRadians(10);			
    	}
	}

    /**
     * onScannedRobot: What to do when you see another robot
     *
     * @param e
     */
	@Override
    public void onScannedRobot(ScannedRobotEvent e) {
	enemy = e;                                                              //salva os dados do robo detectado
	scanTempo = e.getTime();                                                //salva o turno no qual o robo foi detectado
        if (enemy != null) {
            if (isAllied(enemy.getName()) == false) {          //escolhe o tipo de tiro baseado na distancia do robo detectado
                setAdjustGunForRobotTurn(true);
                setTurnGunRight(getHeading() - getGunHeading() + e.getBearing());       //mira no robo detectado
                if (enemy.getDistance() > 300) {
                    setFire(1);
                    setFire(1);
                } else {
                    setFire(3);
                    setFire(3);
                }
            }
	}
    }	

    /**
     * onHitByBullet: What to do when you're hit by a bullet
     *
     * @param e
     */
    @Override
    public void onHitByBullet(HitByBulletEvent e) {
	// Replace the next line with any behavior you would like
	back(80);
	setTurnRight(90);
    }

    /**
     * onHitWall: What to do when you hit a wall
     *
     * @param e
     */
	public void onHitWall(HitWallEvent e){
		back(120);
		turnRight(90);
		ahead(300);
	}	

    // Metodo utilizado no evento onScannedRobot: verifica se o robo scaneado é aliado ou nao
    private boolean isAllied(String scannedRobotName) {
	for (String robot : dontAttack) {
	    if (scannedRobotName.toLowerCase().contains(robot.toLowerCase())) {
		return true;
	    }
	}
	return false;
    }

}